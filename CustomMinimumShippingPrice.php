<?php
/*
Plugin Name: WooCommerce Custom Minimum Shipping Price
Plugin URI: 
Description: This plugin creates a new shipping method for woocommerce
Version: 1.0.0
Author: Vitor Rigoni @ LemonJuice Web Apps
Author URI: http://lemonjuicewebapps.com
*/

/**
 * Check if WooCommerce is active
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	function cmsp_init() {
		require_once("class-custom-minimum-shipping-price.php");
	}

	add_action( 'woocommerce_shipping_init', 'cmsp_init' );

	function add_cmsp( $methods ) {
		$methods[] = 'WC_Custom_Minimum_Shipping_Price';
		return $methods;
	}

	add_filter( 'woocommerce_shipping_methods', 'add_cmsp' );

}