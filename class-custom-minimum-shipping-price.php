<?php 

class WC_Custom_Minimum_Shipping_Price extends WC_Shipping_Method {
	/**
	 * Constructor for your shipping class
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$this->id                 = 'custom_minimum_shipping_price'; // Id for your shipping method. Should be uunique.
		$this->method_title       = __( 'Custom Minimum Shipping Price', 'cmsp_lang' );  // Title shown in admin
		$this->method_description = __( 'Creates a new shipping method that lets you choose a minimum shipping price and a percentage of the cart', 'cmsp_lang' ); // Description shown in admin

		$this->init_form_fields();
		$this->init_settings();

		$this->enabled            = $this->get_option( 'enable_disable' ); // This can be added as an setting but for this example its forced enabled
		$this->title              = $this->get_option( 'title' ); // This can be added as an setting but for this example its forced.
		$this->description 		  = $this->get_option( 'description' );
		$this->minimum_price      = $this->get_option( 'minimum_price' );
		$this->percentage         = $this->get_option( 'percentage' );

		$this->availability       = 'specific';
		$this->countries          = array( 'BR' );

		add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
	}

	public function init_form_fields() {
		$this->form_fields = array(
			'enable_disable' => array(
				'title' => __('Enable/Disable', 'cmsp_lang'),
				'label' => __('Enable/Disable this delivery method', 'cmsp_lang'),
				'type' => 'checkbox',
				'default' => 'yes'
			),
	    	'title' => array(
	        	'title' => __( 'Title', 'woocommerce' ),
	        	'type' => 'text',
	        	'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
	        	'default' => __( 'Custom Shipping Method', 'cmsp_lang' )
	        ),
	     	'description' => array(
	        	'title' => __( 'Description', 'woocommerce' ),
	        	'type' => 'textarea',
	        	'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce' ),
	        	'default' => __("Custom Shipping Method", 'cmsp_lang')
			),
			'minimum_price' => array(
				'title' => __('Minimum Shipping Price', 'cmsp_lang'),
				'type' => 'text',
				'class' => 'wc_input_price',
				'default' => 50
			),
			'percentage' => array(
				'title' => __('Percentage of the cart to add', 'cmsp_lang'),
				'type' => 'text',
				'class' => 'wc_input_price',
				'default' => 5,
				'help' => "asdasdasdas"
			)
	    );
	}

	/**
	 * calculate_shipping function.
	 *
	 * @access public
	 * @param mixed $package
	 * @return void
	 */
	public function calculate_shipping( $package ) {

		$cost = $package['contents_cost'] * ($this->percentage / 100);

		if ($cost < $this->minimum_price) {
			$cost = $this->minimum_price;
		}

		$rate = array(
			'id' => $this->id,
			'label' => $this->title,
			'cost' => $cost
		);

		// Register the rate
		$this->add_rate( $rate );
	}
}




















